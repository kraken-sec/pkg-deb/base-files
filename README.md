**base-files-20180830**

*Package for Kraken-OS, release kraken alpha_20180830*

*forked from base-files 10.1 (debian unstable/sid)*
*and base-files 2018.3.0 (kali-rolling 2018.3)*


# PACKAGING

* Install package :

```sh
sudo apt install build-essential devscripts debhelper fakeroot
```

* Build .deb :

```sh
git clone https://gitlab.com/kraken-sec/pkg-deb/base-files.git
cd base-files/base-files-20180830/
```

>amd64

```sh
 $ debuild -i -uc -us --target-arch=amd64 --build=any
```

>i386

```sh
 $ debuild -i -uc -us --target-arch=i386 --build=any
```

